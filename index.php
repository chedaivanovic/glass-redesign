<?php
$title = ("Glass Service Zamena Stakala | Početna");
$pagelang = ("sr");
$h1 = ("Glass Service | Auto Stakla |  Zamena i reparacija | Beograd");
?>

<?php include('inc/inc-header.php'); ?>
<?php include('inc/inc-menu.php'); ?>

<div class="hero-slider">
    <div class="hero-slide-container">
        <img src="images/glass-service-auto-stakla-hero1.jpg" alt="" class="w-100">
        <div class="wrapper">
            <div class="herotext">
                <p class="h2">Nalazimo se na 9 lokacija u Beogradu</p>
                <p>Saznajte gde nas ima?</p>
                <a href="" class="hero-text-link">Lokacije</a>
            </div>
        </div>
    </div>
    <div class="hero-slide-container">
        <img src="images/glass-service-auto-stakla-hero2.jpg" alt="" class="w-100">
        <div class="wrapper">
            <div class="herotext">
                <p class="h2">Originalna auto stakla</p>
                <p>Saznajte gde nas ima?</p>
                <a href="" class="hero-text-link">Lokacije</a>
            </div>
        </div>
    </div>
    <div class="hero-slide-container">
        <img src="images/glass-service-auto-stakla-hero3.jpg" alt="" class="w-100">
        <div class="wrapper">
            <div class="herotext">
                <p class="h2">Brza Usluga</p>
                <p>Saznajte gde nas ima?</p>
                <a href="" class="hero-text-link">Lokacije</a>
            </div>
        </div>
    </div>
    <div class="hero-slide-container">
        <img src="images/glass-service-auto-stakla-hero4.jpg" alt="" class="w-100">
        <div class="wrapper">
            <div class="herotext">
                <p class="h2">Zamena i reparacija Auto stakala</p>
                <p>Saznajte gde nas ima?</p>
                <a href="" class="hero-text-link">Lokacije</a>
            </div>
        </div>
    </div>
    <div class="hero-slide-container">
        <img src="images/glass-service-auto-stakla-hero5.jpg" alt="" class="w-100">
        <div class="wrapper">
            <div class="herotext">
                <p class="h2">Poliranje Farova</p>
                <p>Saznajte gde nas ima?</p>
                <a href="" class="hero-text-link">Lokacije</a>
            </div>
        </div>
    </div>

</div>

<div class="main-onama">
    <div class="wrapper">
        <div class="row">
            <div class="col-12 col-md-7">
                <h3 class="h6 font-mcolor">Glass Service</h3>
                <p class="h2 naslov-sekcije">O nama</p>
                <hr class="bg-primary hr-under">
                <p class="lead"><span class="font-weight-bold mb-5"> Lorem ipsum dolor sit amet consectetur adipisicing elit.</span> <br> Dolor placeat saepe, odio doloremque necessitatibus ad unde at deleniti pariatur beatae. Atque maxime consequatur facere?<br> Aut voluptatum saepe eveniet explicabo pariatur inventore porro culpa doloribus quasi at magni perferendis, delectus ad! Dolorum consequuntur veritatis harum exercitationem mollitia explicabo quis eveniet enim accusamus aperiam obcaecati dicta aut quod, adipisci ullam ipsam quibusdam? Lorem ipsum, dolor sit amet consectetur adipisicing elit.<br> Id, praesentium asperiores aliquid non nisi aliquam unde, perferendis vero fugiat maiores in! Neque, nam fugiat porro, praesentium aliquam vel optio omnis natus a sint tenetur architecto eum inventore necessitatibus eligendi ab maxime amet ullam numquam. Eveniet nostrum temporibus animi provident corporis!</p>
            </div>
            <div class="col-12 col-md-5">
                <img src="images/glass-service-auto-stakla-main-onama.jpg" alt="">
            </div>
        </div>
    </div>
</div>

<div class="whyus">
    <div class="wrapper">
        <div class="row">
            <div class="col-12 col-md-6">
                <h3 class="h6 text-white">Glass Service</h3>
                <p class="h2 naslov-sekcije text-white">Zašto da odaberete nas?</p>
                <hr class="hr-under bg-white">
            </div>
            <div class="col-12 col-md-6">
                <p class="lead text-white">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Perspiciatis quos provident minus ullam, praesentium minima exercitationem rerum. In facilis, earum esse expedita repellat, doloremque, quam eveniet ab facere sed praesentium. Dolores, esse labore, asperiores facilis officia, natus sequi ea necessitatibus nam qui soluta unde quibusdam.</p>
            </div>
            <hr class="bg-white col-12 mt-5">
            <div class="row col-12 mt-5">
                <a href="" class="col-12-col-sm-6 col-md-3 text-white d-flex justify-content-center align-items-center">
                    <div class="ikonice-whyus mr-3">
                        <img src="images/glass-service-auto-stakla-ikonica-hauba.png" alt="">
                    </div>
                    Zamena Šoferšajbni
                </a>
                <a href="" class="col-12-col-sm-6 col-md-3 text-white d-flex justify-content-center align-items-center">
                    <div class="ikonice-whyus mr-3">
                        <img src="images/glass-service-auto-stakla-ikonica-zadnje.png" alt="">
                    </div>
                    Zamena Zadnjeg stakla
                </a>
                <a href="" class="col-12-col-sm-6 col-md-3 text-white d-flex justify-content-center align-items-center">
                    <div class="ikonice-whyus mr-3">
                        <img src="images/glass-service-auto-stakla-ikonica-vrata.png" alt="">
                    </div>
                    Zamena Vetrobrana
                </a>
                <a href="" class="col-12-col-sm-6 col-md-3 text-white d-flex justify-content-center align-items-center">
                    <div class="ikonice-whyus mr-3">
                        <img src="images/glass-service-auto-stakla-ikonica-auto.png" alt="">
                    </div>
                    Zatamnjivanje svih stakala
                </a>
            </div>

        </div>
    </div>
</div>

<div class="home-call-onroad">
    <div class="wrapper">
        <div class="row">
            <div class="col-12 col-md-8">
                <h3 class="h6 font-mcolor">Glass Service</h3>
                <p class="h2 naslov-sekcije">Pozovite terensku službu</p>
                <hr class="bg-primary hr-under mb-5">
                <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio magni dolorum at accusamus necessitatibus animi perferendis autem omnis porro eaque. Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi commodi esse expedita ab saepe modi sed perspiciatis explicabo quidem! Nesciunt explicabo similique aperiam a eligendi vero laborum corrupti libero sint. </p>
            </div>
            <div class="col-12 col-md-4 d-flex justify-content-around align-items-center">
                <a href="" class="hero-text-link w-50">Pozovite odmah</a>
            </div>
        </div>
    </div>
</div>

<div class="pronadjite-staklo">
    <div class="wrapper">
        <h3 class="h6 text-white">Glass Service</h3>
        <p class="h2 naslov-sekcije text-white">Pronađite svoje staklo</p>
        <hr class="bg-white hr-under mb-5">
        <form action="" class="row justify-space-around">
            <div class="input form-group col-12 col-sm-6 mb-0">
                <select class="select-css sctdd maincol form-control my-3 h-auto py-3" name="" id="">
                    <option value="" disabled="" selected="">TIP VOZILA</option>
                    <option value="">Automobil</option>
                    <option value="">Kamion</option>
                    <option value="">Autobus</option>
                    <option value="">Kombi</option>
                    <option value="">Građevinska mašina</option>
                </select>
            </div>
            <div class="input form-group col-12 col-sm-6 mb-0">
                <select class="select-css sctdd maincol form-control my-3 h-auto py-3" name="" id="">
                    <option value="" disabled="" selected="">TIP STAKLA</option>
                    <option value="">Šoferšajbna</option>
                    <option value="">Zadnje Staklo</option>
                    <option value="">Vetrobran</option>
                </select>
            </div>
            <div class="input col-12 col-sm-6">
                <input class="form_deo form-control py-4 my-3 maincol" type="text" placeholder="Marka Vozila">
            </div>
            <div class="input col-12 col-sm-6">
                <input class="form_deo form-control py-4 my-3 maincol" type="text" placeholder="Tip Vozila">
            </div>
            <button class="mt-4 col-12 btn btn-lg btn-danger w-100 d-block">Pronađite</button>
        </form>
    </div>
</div>

<div class="home-wheels">
    <div class="wrapper">
        <div class="row">
            <div class="col-12 col-md-6">
                <h3 class="h6 font-mcolor">Glass Service</h3>
                <p class="h2 naslov-sekcije">Zašto smo bolji od drugih?</p>

                <hr class="bg-primary hr-under mb-5">

                <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto nobis iure dolore eum debitis, nihil magnam in! Qui doloremque, quisquam laboriosam praesentium dicta quidem inventore!</p>
            </div>
            <div class="bar col-12 col-sm-4 col-md-2" id="bar1">
                <svg class="radial-progress" data-percentage="100" viewBox="0 0 80 80">
                    <circle class="incomplete" cx="40" cy="40" r="35"></circle>
                    <circle class="complete" cx="40" cy="40" r="35" style="stroke-dashoffset: 22;"></circle>
                    <text class="percentage" x="50%" y="57%" transform="matrix(0, 1, -1, 0, 80, 0)">
                        100%
                    </text>
                </svg>
                <p class="h3 font-weight-light">Brza usluga</p>

            </div>
            <div class="bar col-12 col-sm-4 col-md-2" id="bar2">
                <svg class="radial-progress" data-percentage="100" viewBox="0 0 80 80">
                    <circle class="incomplete" cx="40" cy="40" r="35"></circle>
                    <circle class="complete" cx="40" cy="40" r="35" style="stroke-dashoffset: 11;"></circle>
                    <text class="percentage" x="50%" y="57%" transform="matrix(0, 1, -1, 0, 80, 0)">
                        100%
                    </text>
                </svg>
                <p class="h3 font-weight-light">Brz dolazak</p>

            </div>
            <div class="bar col-12 col-sm-4 col-md-2" id="bar3">
                <svg class="radial-progress" data-percentage="100" viewBox="0 0 80 80">
                    <circle class="incomplete" cx="40" cy="40" r="35"></circle>
                    <circle class="complete" cx="40" cy="40" r="35" style="stroke-dashoffset: 132;"></circle>
                    <text class="percentage" x="50%" y="57%" transform="matrix(0, 1, -1, 0, 80, 0)">
                        100%
                    </text>
                </svg>
                <p class="h3 font-weight-light">Najpovoljnije cene</p>
            </div>
        </div>
    </div>
</div>

<div class="home-usluge">
    <div class="wrapper row">
        <div class="col-12 col-lg-3">
            <h3 class="h6 text-white">Glass Service</h3>
            <p class="h2 naslov-sekcije text-white">Usluge</p>

            <hr class="bg-white hr-under mb-5">

            <p class="lead text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur quaerat veniam doloribus vel illo in perspiciatis cum minus sunt nisi?</p>
        </div>
        <div class="row col-12 col-lg-9 justify-content-around">
            <div class="col-12 col-sm-6 col-md-3">
                <div>
                    <a href="" class="usluge-kartica mt-5 mb-3 mt-sm-3 mt-lg-0 mb-lg-0">
                        <img class="w-100" src="images/glass-service-auto-stakla-home-card-reparacija.jpg" alt="">
                        <p>Reparacija stakala</p>
                    </a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 my-3 my-lg-0">
                <div>
                    <a href="" class="usluge-kartica">
                        <img class="w-100" src="images/glass-service-auto-stakla-home-card-zamena.jpg" alt="">
                        <p>Zamena stakala</p>
                    </a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 my-3 my-lg-0">
                <div>
                    <a href="" class="usluge-kartica">
                        <img class="w-100" src="images/glass-service-auto-stakla-home-card-zatamnjivanje.jpg" alt="">
                        <p>Zatamnjivanje stakala</p>
                    </a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 mt-3 mt-lg-0">
                <div>
                    <a href="" class="usluge-kartica">
                        <img class="w-100" src="images/glass-service-auto-stakla-home-card-poliranje.jpg" alt="">
                        <p>Poliranje farova</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>



<?php include('inc/inc-footer.php'); ?>