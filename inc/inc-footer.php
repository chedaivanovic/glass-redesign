  <footer class="bg-white">
     <div class="wrapper">
        <div class="row">
           <div class="col-12 col-sm-6 col-md-3">
              <a href="index.php" class="footer-logo-holder">
                 <img src="images/glass-service-auto-stakla-logo.png" alt="" class="footer-logo">
              </a>
           </div>
           <div class="col-12 col-sm-6 col-md-3">
              <h3 class="font-mcolor footer-headers">Kontakt</h3>
              <p class="lead my-0">Call Centar <a class=" d-inline-block" href="tel:">0800 132 132</a></p>
              <p class="lead my-0">Preševska 33 <a class=" d-inline-block" href="tel:">063/492-004</a></p>
              <p class="lead my-0">Paštrovićeva 12 <a class=" d-inline-block" href="tel:">063/280-080</a></p>
              <p class="lead my-0">Kruž put Voždovački 17a <a class=" d-inline-block" href="tel:">063/492-944</a></p>
              <p class="lead my-0">Prvomajska 22 <a class=" d-inline-block" href="tel:">+38163492482</a></p>
              <p class="lead my-0">Tošin Bunar <a class=" d-inline-block" href="tel:">063/209-500</a></p>
              <p class="lead my-0">Đuje i Dragoljuba 3/1 <a class=" d-inline-block" href="tel:">011/237-03-23</a></p>
              <p class="lead my-0">Ugrinovačka 23 <a class=" d-inline-block" href="tel:">063/492-020</a></p>
              <p class="lead my-0">Benkovačka 1 <a class=" d-inline-block" href="tel:">063415115</a></p>
              <p class="lead my-0">Smederevski put 22b <a class=" d-inline-block" href="tel:">063492014</a></p>
           </div>
           <div class="col-12 col-sm-6 col-md-3"></div>
           <div class="col-12 col-sm-6 col-md-3"></div>
        </div>
     </div>
  </footer>








  <script src="js/slick.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/script.js"></script>

  </body>

  </html>