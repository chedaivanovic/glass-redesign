<header>
    <div class="top-header">
        <div class="wrapper">
            <h1><?php echo ($h1); ?></h1>
            <ul class="top-header-links float-right">
                <li class="d-inline">
                    <a href="">
                        <img src="images/glass-service-auto-stakla-placeholder.svg" class="d-inline" alt="">
                        Pogledajte lokacije
                    </a>
                </li>
                <li class="d-inline">
                    <a href="">
                        <img src="images/glass-service-auto-stakla-call.svg" class="d-inline" alt="">
                        Pogledajte lokacije
                    </a>
                </li>
                <li class="d-inline">
                    <a href="">
                        <img src="images/glass-service-auto-stakla-email.svg" class="d-inline" alt="">
                        Pogledajte lokacije
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-header">
        <div class="wrapper position-relative">
            <a href="index.php" class="logoholder d-inline">
                <img src="images/glass-service-auto-stakla-logo.png" alt="" class="header-logo">
            </a>
            <nav class="main-nav-holder">
                <ul class="main-nav">
                    <li class="nav-item">
                        <a href="onama.php" class="nav-link">
                            <h2 class="text-uppercase">O nama</h2>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            <h2 class="text-uppercase">Sve usluge</h2>
                        </a>
                        <ul class="dropdown dropdown-usluge">
                            <li class="nav-item">
                                <a href="reparacija.php" class="nav-link">
                                    <h3>Reparacija Stakala</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="zamena.php" class="nav-link">
                                    <h3>Zamena Stakala</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="zatamnjivanje.php" class="nav-link">
                                    <h3>Zatamnjivanje stakala</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="poliranje.php" class="nav-link">
                                    <h3>Poliranje farova</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="terenska.php" class="nav-link">
                                    <h3>Terenska služba</h3>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            <h2 class="text-uppercase">Auto Stakla</h2>
                        </a>
                        <ul class="dropdown dropdown-stakla">
                            <li class="nav-item">
                                <a href="automobili.php" class="nav-link">
                                    <h3>Stakla za Automobile</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="kamioni.php" class="nav-link">
                                    <h3>Stakla za kamione</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="kombi.php" class="nav-link">
                                    <h3>Stakla za kombije</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="autobusi.php" class="nav-link">
                                    <h3>Stakla za autobuse</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="gradjmasine.php" class="nav-link">
                                    <h3>Stakla za građevinske mašine</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="original.php" class="nav-link">
                                    <h3>Originalna auto stakla</h3>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            <h2 class="text-uppercase">Proizvodi</h2>
                        </a>
                        <ul class="dropdown dropdown-proizvodi">
                            <li class="nav-item">
                                <a href="auto-stakla.php" class="nav-link">
                                    <h3>Auto stakla</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="senzori.php" class="nav-link">
                                    <h3>Senzori</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="proizvodjaci.php" class="nav-link">
                                    <h3>Proizvođači</h3>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            <h2 class="text-uppercase">Saradnja</h2>
                        </a>
                        <ul class="dropdown dropdown-saradnja">
                            <li class="nav-item">
                                <a href="sindikati.php" class="nav-link">
                                    <h3>Sindikati</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="osiguranja.php" class="nav-link">
                                    <h3>Osiguravajuća društva</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="partneri.php" class="nav-link">
                                    <h3>Partner Servisi</h3>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="reference.php" class="nav-link">
                                    <h3>Reference</h3>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="blog.php" class="nav-link">
                            <h2 class="text-uppercase">Blog</h2>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="kontakt.php" class="nav-link">
                            <h2 class="text-uppercase">Kontakt i lokacije</h2>
                        </a>
                    </li>

                </ul>
            </nav>
            <a class="cta-header-call h4 font-weight-light text-right" href="tel:">Pozovite nas odmah:<br><span class="h1">0800 132 132</span>
            </a>
        </div>
    </div>
</header>